<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Product;
use App\Entity\Upload;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $admin = new User();
        $admin->setSurname("admin");
        $admin->setLastname("admin");
        $admin->setEmail("admin@admin.com");
        $admin->setGender($faker->randomElement($array = array('Monsieur', 'Madame')));
        $admin->setRoles(["ROLE_SUPER_ADMIN"]);
        $admin->setIsActive(1);
        $admin->setPassword($this->encoder->encodePassword($admin, 'password'));
        $manager->persist($admin);

        $publishers = [];
        for ($i = 0; $i <= 5; $i ++){
            $publisher = new User();
            $nb = mt_rand(0,23);
            $publisher->setEmail($faker->email);
            $publisher->setLastname($faker->lastName);
            $publisher->setSurname($faker->lastName);
            $publisher->setIsActive($faker->boolean);
            $publisher->setRoles(["ROLE_PUBLISHER"]);
            $publisher->setGender($faker->randomElement($array = array('Monsieur', 'Madame')));
            $publisher->setPassword($this->encoder->encodePassword($publisher, 'password'));
            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        // gestion des users
        $users = [];
        for ($i = 0; $i <= 5; $i ++) {
            $user = new User();
            $nb = mt_rand(0, 23);
            $user->setEmail($faker->email);
            $user->setLastname($faker->lastName);
            $user->setSurname($faker->lastName);
            $user->setIsActive($faker->boolean);
            $user->setRoles(["ROLE_USER"]);
            $user->setGender($faker->randomElement($array = array('Monsieur', 'Madame')));
            $user->setPassword($this->encoder->encodePassword($user, 'password'));
            $manager->persist($user);
            $users[] = $user;
        }

        //2/3 catégories
        for ($i = 0; $i <= 2; $i ++) {
            $tag = new Category();
            $tag->setName($faker->sentence($nbWords = 2, $variableNbWords = true));
            $tags [] = $tag;
            $manager->persist($tag);

            // créer 2 articles / catégorie
            for ($j = 0; $j <= 2; $j++) {
                $product = new Product();
                $publisher = $publishers[mt_rand(0, count($publishers) - 1)];
                $title = $faker->sentence();
                $product->setName($title)
                    ->setPrice(mt_rand(13, 140))
                    ->setPromo($faker->boolean)
                    ->setOnline($faker->boolean)
                    ->setOwner($publisher)
                    ->setPublishedAt(new \DateTime())
                    ->setContent($faker->paragraph(4))
                    ->setDescription($faker->paragraph(2));
                    foreach ($tags as $tag){
                        $product->addCategory($tag);
                    }
                $manager->persist($product);

                 // création de commentaire par produit
                for ($k = 1; $k <= mt_rand(4, 10); $k++) {
                    $review = new Comment();
                    $user = $users[mt_rand(0, count($users) - 1)];
                    $review->setUser($user)
                        ->setContent($faker->paragraph(5))
                        ->setProduct($product)
                        ->setCreatedAt(new \DateTime());
                    $manager->persist($review);
                }

                for( $l = 0; $l <= mt_rand(1,6); $l++){
                    $picture = new Upload('/public/uploads/products/Abstract.png');
                    $picture->setProduct($product);
                    $picture->setFilename("Abstract.png");

                    $manager->persist($picture);
                }
            }
        }
        $manager->flush();
    }
}
