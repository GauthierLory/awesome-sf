<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',EmailType::class,['attr' => ['placeholder' => 'title.user_mail'] ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'user.invalid_message',
                'required' => true,
                'mapped' => false,
                'first_options' => ['attr' => ['placeholder' => 'title.user_password']],
                'second_options' => ['attr' => ['placeholder' => 'title.user_password_confirm']],
            ))
            ->add('surname', TextType::class,['attr' => ['placeholder' => 'title.user_surname'] ])
            ->add('lastname',TextType::class,['attr' => ['placeholder' => 'title.user_lastname'] ])
            ->add('gender', ChoiceType::class,array(
                    'choices' => [
                        'title.user_gender_female' => 'title.user_gender_female',
                        'title.user_gender_male' => 'title.user_gender_male',
                    ])
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
