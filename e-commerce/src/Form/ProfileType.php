<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',EmailType::class,['attr' => ['placeholder' => 'title.user_mail'] ])
            ->add('surname', TextType::class,['attr' => ['placeholder' => 'title.user_surname'] ])
            ->add('lastname',TextType::class,['attr' => ['placeholder' => 'title.user_lastname'] ])
            ->add('gender', ChoiceType::class,array(
                    'choices' => [
                        'title.user_gender_female' => 'title.user_gender_female',
                        'title.user_gender_male' => 'title.user_gender_male',
                    ])
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
