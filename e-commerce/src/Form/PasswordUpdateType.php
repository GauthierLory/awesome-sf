<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PasswordUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, array(
                'attr' => ['placeholder' => 'profile.old_password']
            ))
            ->add('newPassword', PasswordType::class, array(
                'attr' => ['placeholder' => "profile.new_password"]
            ))
            ->add('confirmPassword', PasswordType::class, array(
                'attr' => ['placeholder' => "profile.confirm_password"]
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}