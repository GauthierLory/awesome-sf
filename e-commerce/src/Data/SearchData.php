<?php


namespace App\Data;


class SearchData
{
    /**
     * @var int
     */
    public $page =1;

    /**
     * @var string
     */
    public $q = '';

    /**
     * @var array
     */
    public $categories= [];

    /**
     * @var integer
     */
    public $max;

    /**
     * @var null|integer
     */
    public $min;

    /**
     * @var boolean
     */
    public $promo = false;
}