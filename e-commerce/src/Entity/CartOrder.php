<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartOrderRepository")
 */
class CartOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="smallint")
     */
    private $items_total;

    /**
     * @ORM\Column(type="integer")
     */
    private $items_price_total;

    /**
     * @ORM\Column(type="integer")
     */
    private $price_total;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CartOrderItem", mappedBy="order")
     */
    private $cartOrderItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CartDiscount", inversedBy="cartOrders")
     */
    private $discount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="cartOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

//    public function __toString() {
//        return "some string representation of your object";
//    }

    public function __construct()
    {
        $this->cartOrderItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getItemsTotal(): ?int
    {
        return $this->items_total;
    }

    public function setItemsTotal(int $items_total): self
    {
        $this->items_total = $items_total;

        return $this;
    }

    public function getItemsPriceTotal(): ?int
    {
        return $this->items_price_total;
    }

    public function setItemsPriceTotal(int $items_price_total): self
    {
        $this->items_price_total = $items_price_total;

        return $this;
    }

    public function getPriceTotal(): ?int
    {
        return $this->price_total;
    }

    public function setPriceTotal(int $price_total): self
    {
        $this->price_total = $price_total;

        return $this;
    }

    /**
     * @return Collection|CartOrderItem[]
     */
    public function getCartOrderItems(): Collection
    {
        return $this->cartOrderItems;
    }

    public function addCartOrderItem(CartOrderItem $cartOrderItem): self
    {
        if (!$this->cartOrderItems->contains($cartOrderItem)) {
            $this->cartOrderItems[] = $cartOrderItem;
            $cartOrderItem->setOrderId($this);
        }

        return $this;
    }

    public function removeCartOrderItem(CartOrderItem $cartOrderItem): self
    {
        if ($this->cartOrderItems->contains($cartOrderItem)) {
            $this->cartOrderItems->removeElement($cartOrderItem);
            // set the owning side to null (unless already changed)
            if ($cartOrderItem->getOrderId() === $this) {
                $cartOrderItem->setOrderId(null);
            }
        }

        return $this;
    }

    public function getDiscount(): ?CartDiscount
    {
        return $this->discount;
    }

    public function setDiscount(?CartDiscount $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
