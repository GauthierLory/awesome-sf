<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Product::class);
        $this->paginator = $paginator;
    }

    /**
     *Récupère les produits en lien avec une recherche
     * @param SearchData $search
     * @return PaginationInterface
     */
    public function findSearch(SearchData $search): PaginationInterface
    {
        $query = $this->getSearchQuery($search)->getQuery();
        return $this->paginator->paginate(
            $query,
            $search->page,
            15
        );
    }

    /**
     * @param SearchData $search
     * @return integer[]
     */
    public function findMinMax(SearchData $search): array
    {
        $result = $this->getSearchQuery($search, true)
            ->select('MIN(p.price) as min', 'MAX(p.price) as max')
            ->andWhere('p.online = 1')
            ->getQuery()
            ->getScalarResult();
        return [(int)$result[0]['min'],(int)$result[0]['max']];
    }

    private function getSearchQuery(SearchData $search, $ignorePage = false): QueryBuilder
    {
        $query = $this
            ->createQueryBuilder('p')
            ->select('c','p')
            ->andWhere('p.online = 1')
            ->join('p.categories','c');

        if (!empty($search->q)){
            $query = $query
                ->where('p.name LIKE :q')
                ->where('p.description LIKE :q')
                ->andWhere('p.online = 1')
                ->setParameter('q', "%{$search->q}%");
        }

        if (!empty($search->min) && $ignorePage === false ){
            $query = $query
                ->andWhere('p.price >= :min')
                ->andWhere('p.online = 1')
                ->setParameter('min', $search->min);
        }

        if (!empty($search->max) && $ignorePage === false ){
            $query = $query
                ->andWhere('p.price <= :max')
                ->andWhere('p.online = 1')
                ->setParameter('max', $search->max);
        }

        if (!empty($search->promo)){
            $query = $query
                ->andWhere('p.promo =1')
                ->andWhere('p.online = 1');
        }

        if (!empty($search->categories)){
            $query = $query
                ->andWhere('c.id IN (:categories)')
                ->andWhere('p.online = 1')
                ->setParameter('categories', $search->categories);
        }

        return $query;
    }
}
