<?php

namespace App\Repository;

use App\Entity\CartOrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CartOrderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartOrderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartOrderItem[]    findAll()
 * @method CartOrderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartOrderItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartOrderItem::class);
    }

    // /**
    //  * @return CartOrderItem[] Returns an array of CartOrderItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CartOrderItem
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
