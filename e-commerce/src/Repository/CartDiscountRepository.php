<?php

namespace App\Repository;

use App\Entity\CartDiscount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CartDiscount|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartDiscount|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartDiscount[]    findAll()
 * @method CartDiscount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartDiscountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartDiscount::class);
    }

    // /**
    //  * @return CartDiscount[] Returns an array of CartDiscount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CartDiscount
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
