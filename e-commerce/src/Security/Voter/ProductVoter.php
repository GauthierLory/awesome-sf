<?php

namespace App\Security\Voter;

use App\Entity\Product;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ProductVoter extends Voter
{
    const EDIT   = 'edit';
    const DELETE = 'delete';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Product) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($this->security->isGranted('ROLE_ADMIN')){
            return true;
        }

        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        if (null == $subject->getOwner()){
            return false;
        }

        /**
         * @var Product $subject
         */
        $product = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($product, $user);
            case self::DELETE:
                return $this->canDelete($product, $user);
        }
        throw new \LogicException('This code should not be reached!');
    }

    private function canDelete(Product $product, User $user)
    {
        // if they can edit, they can delete
        if ($this->canEdit($product, $user)) {
            return true;
        }else return false;

    }

    private function canEdit(Product $product, User $user)
    {
        return $user === $product->getOwner();
    }
}
