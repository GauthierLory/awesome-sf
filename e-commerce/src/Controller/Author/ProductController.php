<?php

namespace App\Controller\Author;

use App\Entity\Product;
use App\Entity\Upload;
use App\Entity\User;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Service\File\FileUploader;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{

    /**
     * @Route("user/product/", name="author_product_index")
     * @param ProductRepository $products
     * @return Response
     */
    public function indexProduct(ProductRepository $products)
    {
        $this->denyAccessUnlessGranted('ROLE_PUBLISHER');

        $user = $this->getUser();
        return $this->render('product/author/index.html.twig',[
            'products' => $products->findBy([ 'owner' => $user])
        ]);
    }

    /**
     * @Route("user/product/{id}/", name="author_product_show")
     * @param Product $product
     * @return Response
     */
    public function showProduct(Product $product)
    {
        $this->denyAccessUnlessGranted('ROLE_PUBLISHER');

        return $this->render('product/author/show.html.twig',[
            'product' => $product
        ]);
    }

    /**
     * @Route("product/new", name="author_product_new")
     * @param Request $request
     * @param ObjectManager $manager
     * @return Response
     * @throws Exception
     */
    public function newProduct(Request $request, ObjectManager $manager)
    {
        $this->denyAccessUnlessGranted('ROLE_PUBLISHER');

        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $product->setPublishedAt(new \DateTime());
            $product->setOwner($this->getUser());
            $manager->persist($product);
            $manager->flush();

            return  $this->redirectToRoute('author_upload',['id' => $product->getId()]);
        }

        return $this->render('product/author/new.html.twig',[
            'form_product' => $form->createView()
        ]);
    }

    /**
     * @Route("product/{id}/edit", name="author_product_edit")
     * @param Request $request
     * @param ObjectManager $manager
     * @param Product $product
     * @return Response
     */
    public function editProduct(Request $request, ObjectManager $manager, Product $product)
    {
        $this->denyAccessUnlessGranted('edit', $product);

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $manager->persist($product);
            $manager->flush();
        }

        return $this->render('product/author/edit.html.twig',[
            'product' =>$product,
            'form_product' => $form->createView()
        ]);
    }

    /**
     * @Route("product/{id}/delete",name="author_product_delete")
     * @param Request $request
     * @param Product $product
     * @return RedirectResponse
     */
    public function deleteProduct(Request $request, Product $product)
    {
        $this->denyAccessUnlessGranted('delete', $product);

        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        }
        return $this->redirectToRoute('author_product_index');
    }

    /**
     * @Route("/upload/product/{id}", name="author_upload")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @param Product $product
     * @return Response
     */
    public function upload(Request $request, FileUploader $fileUploader,Product $product)
    {
        $this->denyAccessUnlessGranted('edit', $product);

        if ($request->isMethod('POST')) {
            $files = $request->files->get('upload')['upload'];
            foreach ($files as $file) {
                $upload = new Upload();
                $fileName = $fileUploader->upload($file);
                $upload->setFilename($fileName);
                $upload->setProduct($product);

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($upload);
                $manager->flush();
            }
            return  $this->redirectToRoute('author_product_show',['id' => $product->getId()]);
        }
        return $this->render('product/upload/index.html.twig',[
            'product_id' => $product->getId()
        ]);
    }
}
