<?php

namespace App\Controller;

use App\Entity\CartOrder;
use App\Entity\CartOrderItem;
use App\Entity\PasswordUpdate;
use App\Form\PasswordUpdateType;
use App\Form\ProfileType;
use App\Repository\CartOrderRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Class ProfileController
 * @package App\Controller
 */
class ProfileController extends AbstractController
{
    /**
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function editPassword(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = $this->getUser();
        $passwordUpdate = new  PasswordUpdate();
        $formPassword = $this->createForm(PasswordUpdateType::class, $passwordUpdate);
        $formPassword->handleRequest($request);

        if ($formPassword->isSubmitted() && $formPassword->isValid()){
            if (!password_verify($passwordUpdate->getOldPassword(), $user->getPassword())){
                $this->addFlash(
                    'warning',"Impossible de changer le mot de passe !"
                );
            }else{
                $newPassword = $passwordUpdate->getNewPassword();
                $hash = $encoder->encodePassword($user, $newPassword);

                $user->setPassword($hash);
                $manager->persist($user);
                $manager->flush();
                $this->addFlash(
                    'success',"Votre mdp est été modifié"
                );
            }
        }

        return $this->render('profile/password.html.twig', [
            'controller_name' => 'ProfileController',
            'formPassword' => $formPassword->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editPersonal(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @return Response
     */
    public function getBill()
    {
        $user = $this->getUser();
        $invoices = $this->getDoctrine()->getRepository(CartOrder::class)->findBy(['user' => $user]);

        return $this->render('profile/invoice_index.html.twig', [
            'controller_name' => 'ProfileController',
            'invoices' => $invoices
        ]);
    }

    /**
     * @Security("is_granted('ROLE_USER') and user == cartOrder.getUser()", message="Cette facture n'est pas la votre !")
     * @param $id
     * @param CartOrderRepository $cartOrderRepository
     * @param CartOrder $cartOrder
     * @return void
     */
    public function downloadInvoice($id, CartOrderRepository $cartOrderRepository, CartOrder $cartOrder)
    {
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

//        $invoice = $this->getDoctrine()->getRepository(CartOrder::class)->findOneBy(['id' => $id]);
        $invoice = $cartOrderRepository->findOneBy(['id' => $id]);
        $items = $this->getDoctrine()->getRepository(CartOrderItem::class)->findBy(['order' => $invoice]);

        $html = $this->renderView('profile/_invoice.html.twig', [
            'invoice' => $invoice,
            'items' => $items
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);
    }
}
