<?php
// src/Controller/AdminController.php
namespace App\Controller\Admin;

use App\Entity\CartOrder;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminController extends EasyAdminController
{
    /**
     * @Route("/admin/login", name="admin_account_login", methods={"GET","POST"} )
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function loginAdmin(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@EasyAdmin/page/login.html.twig', [
            // parameters usually defined in Symfony login forms
            'error' => $error,
            'last_username' => $lastUsername,

            // OPTIONAL parameters to customize the login form:

            // the string used to generate the CSRF token. If you don't define
            // this parameter, the login form won't include a CSRF token
            'csrf_token_intention' => 'authenticate',
            // the URL users are redirected to after the login (default: path('easyadmin'))
            'target_path' => $this->generateUrl('easyadmin'),
            // the label displayed for the username form field (the |trans filter is applied to it)
            'username_label' => 'Your username',
            // the label displayed for the password form field (the |trans filter is applied to it)
            'password_label' => 'Your password',
            // the label displayed for the Sign In form button (the |trans filter is applied to it)
            'sign_in_label' => 'Log in',
            // the 'name' HTML attribute of the <input> used for the username field (default: '_username')
            'username_parameter' => '_username',
            // the 'name' HTML attribute of the <input> used for the password field (default: '_password')
            'password_parameter' => '_password',
        ]);
    }

    /**
     * @Route("/admin/dashboard", name="admin_dashboard")
     */
    public function dashboard(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repoProduct = $em->getRepository(Product::class);
        $repoUser = $em->getRepository(User::class);
        $repoOrder = $em->getRepository(CartOrder::class);

        $totalProductOnline = $repoProduct->createQueryBuilder('p')
            ->select('count(p.id)')
            ->andWhere('p.online = 1')
            ->getQuery()
            ->getSingleScalarResult();

        $totalProduct = $repoProduct->createQueryBuilder('p')
            ->select('count(p.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $totalUser = $repoUser->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $totalUserActive = $repoUser->createQueryBuilder('u')
            ->select('count(u.id)')
            ->andWhere('u.isActive = 1')
            ->getQuery()
            ->getSingleScalarResult();

        $totalOrder = $repoOrder->createQueryBuilder('o')
            ->select('count(o.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $totalAmountOrder = $repoOrder->createQueryBuilder('o')
            ->select('sum(o.price_total)')
            ->getQuery()
            ->getSingleScalarResult();


        return $this->render('admin/dashboard.html.twig',[
            'totalProduct' => $totalProduct,
            'totalProductOnline' => $totalProductOnline,
            'totalUser' => $totalUser,
            'totalUserActive' => $totalUserActive,
            'totalAmountOrder' => $totalAmountOrder,
            'totalOrder' => $totalOrder,
        ]);
    }
}