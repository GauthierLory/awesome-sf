<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Comment;
use App\Entity\Product;
use App\Form\CommentType;
use App\Form\SearchForm;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;

class ProductController extends AbstractController
{
    /**
     * @param ProductRepository $repository
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function index(ProductRepository $repository, Request $request)
    {
        $data = new SearchData();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(SearchForm::class, $data);
        $form->handleRequest($request);

        [$min, $max] = $repository->findMinMax($data);
        $products = $repository->findSearch($data);

        if ($request->get('ajax')){
            return new JsonResponse([
               'content' => $this->renderView('product/_products.html.twig', ['products' => $products ]),
               'sorting' => $this->renderView('product/_sorting.html.twig', ['products' => $products ]),
               'pagination' => $this->renderView('product/_pagination.html.twig', ['products' => $products ]),
                'page' => ceil($products->getTotalitemCount() / $products->getItemNumberPerPage()),
                'min' => $min,
                'max' => $max,
            ]);
        }

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'form' => $form->createView(),
            'min' => $min,
            'max' => $max
        ]);
    }

    /**
     * @param ProductRepository $productRepository
     * @param $slug
     * @return Response
     * @Route("/produit/{slug}/", name="product_show")
     * @ParamConverter("product", options={"mapping": {"productSlug": "slug"}})
     */
    public function showProduct($slug,ProductRepository $productRepository)
    {

        $product = $productRepository->findOneBy(['slug' => $slug]);

        return $this->render('product/show.html.twig',[
            'product' => $product
        ]);
    }

    /**
     * @param Product $product
     * @param Request $request
     * @return Response
     * @throws Exception
     * @ParamConverter("product", options={"mapping": {"productSlug": "slug"}})
     */
    public function commentNew(Product $product, Request $request)
    {
        $comment = new Comment();

        $product->addComment($comment);

            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
            $comment->setUser($this->getUser());
            $comment->setProduct($product);
            $comment->setCreatedAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('product_show', ['slug' => $product->getSlug()]);
        }

        return $this->render('product/comment_form_error.html.twig',[
            'product' => $product,
            'form' => $form->createView()
        ]);
    }

    public function commentForm(Product $product): Response
    {
        $form = $this->createForm(CommentType::class);

        return $this->render('product/_comment_form.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }
}
