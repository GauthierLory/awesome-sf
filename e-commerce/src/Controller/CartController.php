<?php

namespace App\Controller;

use App\Entity\CartDiscount;
use App\Entity\CartOrder;
use App\Entity\CartOrderItem;
use App\Entity\Product;
use App\Repository\CartDiscountRepository;
use App\Service\Cart\CartService;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CartController extends AbstractController
{
    /**
     * @param CartService $cartService
     * @return Response
     */
    public function index(CartService $cartService)
    {
        return $this->render('cart/index.html.twig', [
            'items' => $cartService->getFullCart(),
            'total' => $cartService->getTotal()
        ]);
    }

    /**
     * @param $id
     * @param CartService $cartService
     * @return RedirectResponse
     */
    public function addProduct($id, CartService $cartService)
    {
        $cartService->addProduct($id);
        return $this->redirectToRoute('cart_index');
    }

    /**
     * @param $id
     * @param CartService $cartService
     * @return RedirectResponse
     */
    public function removeItem($id, CartService $cartService)
    {
        $cartService->removeItem($id);
        return $this->redirectToRoute('cart_index');
    }

    /**
     * @param $id
     * @param CartService $cartService
     * @return RedirectResponse
     */
    public function removeProduct($id, CartService $cartService)
    {
        $cartService->removeProduct($id);
        return $this->redirectToRoute('cart_index');
    }

    /**
     * @param CartService $cartService
     * @return RedirectResponse
     */
    public function removeCart(CartService $cartService)
    {
        $cartService->removeCart();
        return $this->redirectToRoute('cart_index');
    }
}
