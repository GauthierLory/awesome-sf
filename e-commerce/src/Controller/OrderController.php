<?php

namespace App\Controller;

use App\Entity\CartOrder;
use App\Entity\CartOrderItem;
use App\Entity\Product;
use App\Service\Cart\CartService;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Stripe\Charge;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class OrderController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/order", name="order")
     * @param CartService $cartService
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function index(CartService $cartService, ObjectManager $manager, Request $request)
    {
        $cart = $cartService->getFullCart();

        $totalOrderNoTax = 0;
        $quantityOrderItems = 0;

        $form = $this->createFormBuilder()
            ->add('token', HiddenType::class, [
                'constraints' => [new NotBlank()],
            ])
            ->add('submit', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = new CartOrder();
            // je parcours mon panier, avec différents produits
            foreach ($cart as $items){
                $orderItem = new CartOrderItem();

                $totalItem = $items['product']->getPrice() * $items['quantity'];
                $totalOrderNoTax += $totalItem;
                $product = $this->getDoctrine()->getRepository(Product::class)->find($items['product']->getId());

                $itemsTotalCart = $items['quantity'];
                $quantityOrderItems += $itemsTotalCart;
                // je persiste chaque produit de mon panier en bdd
                $orderItem->setProduct($product);
                $orderItem->setCreatedAt(new \DateTime());
                $orderItem->setQuantity($quantityOrderItems);
                $orderItem->setPriceTotal($totalItem);
                $orderItem->setOrder($order);
                $manager->persist($orderItem);
            }

            // je créer une nouvelle commande dans la bdd
            $totalOrderTax = $totalOrderNoTax * 1.20;
            $order->setItemsTotal($quantityOrderItems);
            $order->setItemsPriceTotal($totalOrderNoTax);
            $order->setPriceTotal($totalOrderTax);
            $order->setCreatedAt(new \DateTime());
            $order->setUser($this->getUser());

            if ($cart == null) {
                $this->addFlash('warning',('Vous n\'avez pas de produits dans votre panier !'));
            }
            else{
                $manager->persist($order);
                $manager->flush();
                $this->addFlash('success',('Commander avec succès !'));
            }

            try {
                $stripe_private_key = $this->getParameter('stripe_secret_key');
                Stripe::setApiKey($stripe_private_key);

                $token = $form->get('token')->getData();
                Charge::create([
                    'amount' => $totalOrderTax * 100,
                    'currency' => 'eur',
                    'description' => 'Example charge',
                    'source' => $token,
                ]);
            } catch (\Stripe\Error\Base $e) {
                $this->addFlash('danger', '');
                throw $e;
            }

            return $this->redirectToRoute('profile_invoice');
        }

        return $this->render('order/index.html.twig', [
            'form' => $form->createView(),
            'stripe_public_key' => $this->getParameter('stripe_public_key')
        ]);
    }
}
