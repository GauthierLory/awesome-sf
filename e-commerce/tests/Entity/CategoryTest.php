<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class CategoryTest extends KernelTestCase
{

    public function getEntity(): Category
    {
        return (new Category());
    }

    public function assertHasErrors(Category $code, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($code);
        $messages = [];
        /**
         * @var ConstraintViolation $error
         */
        foreach ($errors as $error){
            $messages[] = $error->getPropertyPath(). ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(',', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity()->setName('porque'),0);
    }

    public function testInvalidNameEntity()
    {
        $this->assertHasErrors($this->getEntity()->setName('titre1'),1);
        $this->assertHasErrors($this->getEntity()->setName('1titre'),1);
    }
}
