<?php

namespace App\Tests\Entity;

use App\Entity\Comment;
use App\Entity\Product;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class CommentTest extends KernelTestCase
{

    public function getEntity(): Comment
    {
        $user = (new User());
        $product = (new Product());

        return (new Comment())
            ->setUser($user)
            ->setProduct($product)
            ->setCreatedAt(new \DateTime());
    }

    public function assertHasErrors(Comment $code, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($code);
        $messages = [];
        /**
         * @var ConstraintViolation $error
         */
        foreach ($errors as $error){
            $messages[] = $error->getPropertyPath(). ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(',', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity()->setContent('azerty'),0);
    }

    public function testInvalidEntity()
    {
        $this->assertHasErrors($this->getEntity()->setContent('@wordincorrect'),1);
    }

    public function testInvalidBlankEntity()
    {
        $this->assertHasErrors($this->getEntity(),1);
    }
}
