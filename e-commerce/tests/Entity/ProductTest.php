<?php

namespace App\Tests\Entity;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class ProductTest extends KernelTestCase
{

    public function getEntity(): Product
    {
        return (new Product());
    }

    public function assertHasErrors(Product $code, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($code);
        $messages = [];
        /**
         * @var ConstraintViolation $error
         */
        foreach ($errors as $error){
            $messages[] = $error->getPropertyPath(). ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(',', $messages));
    }

    public function testInvalidEntity()
    {
        $this->assertHasErrors(
            $this->getEntity()
            ->setName('title')
            ->setDescription('moins de 30 carac bla bla bla')
            ->setContent('moins de 30carac')
            ->setPrice(-1),
            3);
    }

    public function testValidEntity()
    {
        $this->assertHasErrors(
            $this->getEntity()
                ->setName('title')
                ->setDescription('Une super description de +30 carac')
                ->setContent('Voici un contenu totalement amazing de plus de 30carac')
                ->setPrice(10),
                0);
    }

}
