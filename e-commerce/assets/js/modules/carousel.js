class Carousel {

    /**
     * @callback moveCallback
     * @param {number} index
     */

    /**
     *
     * @param  {HTMLElement}element
     * @param {Object} options
     * @param {Object} [options.slidesToScroll=1] Nombre d'éléments à faire défiler
     * @param {Object} [options.slidesVisible=1] Nombre d'éléments visible
     * @param {boolean} [options.loop=false] Doit-on boucler en fin de slide ?
     */
    constructor(element, options = {}) {
        this.element = element;
        this.options = Object.assign({},{
            slidesToScroll: 1,
            slidesVisible: 1,
            loop: false
        }, options);
        let children = [].slice.call(element.children);
        this.isMobile = false;
        this.currentItem = 0;
        this.root = this.createDivWithClass('carousel');
        this.container = this.createDivWithClass('carousel__container');
        this.root.appendChild(this.container);
        this.element.append(this.root);
        this.moveCallbacks = [];
        this. items = children.map((child) => {
            let item = this.createDivWithClass('carousel__item');
            item.appendChild(child);
            this.container.appendChild(item);
            return item;
        });
        this.setStyle();
        this.createNavigation();
        this.moveCallbacks.forEach(cb => cb(0));
        this.onWindowsResize();
        window.addEventListener('resize', this.onWindowsResize.bind(this))
    }

    setStyle(){
        let ratio = this.items.length / this.slidesVisible;
        this.container.style.width = (ratio* 100) + "%";

        this.items.forEach(item => item.style.width = item.style.width =  ((100 / this.slidesVisible) / ratio ) + "%");
    }

    createNavigation(){
        let nextButton = this.createDivWithClass('carousel__next');
        let prevButton = this.createDivWithClass('carousel__prev');
        this.root.appendChild(nextButton);
        this.root.appendChild(prevButton);
        nextButton.addEventListener('click',this.next.bind(this));
        prevButton.addEventListener('click',this.prev.bind(this));
        if (this.options.loop === true){
            return
        }
        this.onMove(index =>{
            if (index === 0){
                prevButton.classList.add('carousel__prev-hidden')
            }else{
                prevButton.classList.remove('carousel__prev-hidden')
            }
            if (this.items [this.currentItem + this.slidesVisible] === undefined){
                nextButton.classList.add('carousel__next-hidden')
            }else {
                nextButton.classList.remove('carousel__next-hidden')
            }
        })
    }

    next(){
        this.gotoItem(this.currentItem + this.slidesToScroll)
    }

    prev(){
        this.gotoItem(this.currentItem - this.slidesToScroll)
    }

    /**
     * Déplace le carousel vers l'élément ciblé
     * @param {number}index
     */
    gotoItem(index){
        if (index <0 ){
            index = this.items.length - this.options.slidesVisible
        }else if (index >= this.items.length || (this.items [this.currentItem + this.options.slidesVisible] === undefined  && index > this.currentItem )){
            index = 0
        }
        let translateX = index * -100 / this.items.length;
        this.container.style.transform = 'translate3d('+ translateX + '% , 0, 0)';
        this.currentItem = index;
        this.moveCallbacks.forEach(cb => cb(index))
    }

    /**
     * @param {moveCallback} cb
     */
    onMove(cb){
        this.moveCallbacks.push(cb);
    }

    onWindowsResize(){
        let mobile = window.innerWidth < 800;
        if (mobile !== this.isMobile){
            this.isMobile = mobile;
            this.setStyle();
            this.moveCallbacks.forEach(cb => cb(this.currentItem));
        }
    }

    /**
     * @param className
     * @returns {HTMLDivElement}
     */
    createDivWithClass (className){
        let div = document.createElement('div');
        div.setAttribute('class', className);
        return div;
    }

    /**
     * @returns {number}
     */
    get slidesToScroll(){
        return this.isMobile ? 1 : this.options.slidesToScroll
    }

    /**
     * @returns {number}
     */
    get slidesVisible(){
        return this.isMobile ? 1 : this.options.slidesVisible
    }
}

document.addEventListener('DOMContentLoaded', function () {
    new Carousel(document.querySelector('#carousel1'),{
        slidesVisible: 1,
        slidesToScroll: 1,
        loop: true
    })
});